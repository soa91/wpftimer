﻿using WpfTimer.Interfaces;
using WpfTimer.Models;

namespace WpfTimer.Services
{
    public class DataReadService : IServiceAdapter
    {
        private readonly JsonFileService _jsonFileService;

        public DataReadService()
        {
            _jsonFileService = new JsonFileService();
        }

        public void Open(string filePath)
        {
            DbContext.SetInstance(_jsonFileService.Open(filePath));
        }

        public void Save(string filePath)
        {
            _jsonFileService.Save(filePath, DbContext.GetInstance());
        }
    }
}
