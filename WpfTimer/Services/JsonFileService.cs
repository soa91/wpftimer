﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using WpfTimer.Interfaces;
using WpfTimer.Models;

namespace WpfTimer.Services
{
    public class JsonFileService : IFileService<DbContext>
    {
        public DbContext Open(string fileName)
        {
            DbContext doc;

            using (FileStream reader = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(DbContext));
                doc = (DbContext)ser.ReadObject(reader);
            }

            return doc;
        }

        public void Save(string fileName, DbContext reps)
        {
            var serializer = new DataContractJsonSerializer(typeof(DbContext));

            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = JsonReaderWriterFactory.CreateJsonWriter(fs, Encoding.UTF8, true, true, "  "))
                {
                    serializer.WriteObject(writer, reps);
                }
            }
        }
    }
}
