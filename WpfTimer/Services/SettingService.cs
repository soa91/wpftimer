﻿using WpfTimer.Interfaces;
using WpfTimer.Models;

namespace WpfTimer.Services
{
    public class SettingService : IModel
    {
        public string Time
        {
            get { return DbContext.GetInstance().Time; }
            set { DbContext.GetInstance().Time = value; }
        }

        public int Interval
        {
            get { return DbContext.GetInstance().Interval; }
            set { DbContext.GetInstance().Interval = value; }
        }

        public string Path
        {
            get { return DbContext.GetInstance().Path; }
            set { DbContext.GetInstance().Path = value; }
        }

        public void Reset()
        {
            DbContext.SetInstance(new DbContext());
        }
    }
}
