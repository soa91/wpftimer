﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTimer.Services
{
    public class SimpleLoger
    {
        public static void Write(string message)
        {
            var fileName = "1.log";

            using (StreamWriter sw = new StreamWriter(fileName, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(message);
            }
        }
    }
}
