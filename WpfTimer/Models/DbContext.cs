﻿using System.Runtime.Serialization;

namespace WpfTimer.Models
{
    [DataContract]
    public class DbContext
    {
        #region singelton
        //Реализация синглтона вместо статики, 
        //чтоб удобно было делать сериализацию
        private static DbContext _dbModel = new DbContext();

        public static DbContext GetInstance()
        {
            return _dbModel;
        }

        public static void SetInstance(DbContext newModel)
        {
            _dbModel = newModel;
        }
        #endregion
        [DataMember]
        public string Time { get; set; }
        [DataMember]
        public int Interval { get; set; }
        [DataMember]
        public string Path { get; set; }

        public DbContext()
        {
            Time = "00:00:00";
            Interval = 1;
            Path = "";
        }
    }
}
