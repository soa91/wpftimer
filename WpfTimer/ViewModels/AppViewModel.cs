﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using Caliburn.Micro;
using WpfTimer.Interfaces;

namespace WpfTimer.ViewModels
{
    [Export(typeof(AppViewModel))]
    public class AppViewModel : PropertyChangedBase
    {
        private IWindowManager _windowManager;
        private IModel _model;
        private IDialogService _dialogService;
        private IServiceAdapter _readService;


        private string _time;
        public string Time
        {
            get { return _time; }
            set
            {
                if (_time != value)
                {
                    _time = value;
                    NotifyOfPropertyChange(() => Time);
                }
            }
        }

        private string _interval;
        public string Interval
        {
            get { return _interval; }
            set
            {
                if (_interval != value)
                {
                    _interval = value;
                    NotifyOfPropertyChange(() => Interval);
                }
            }
        }

        private string _path;
        public string Path
        {
            get { return _path; }
            set
            {
                if (_path != value)
                {
                    _path = value;
                    NotifyOfPropertyChange(() => Path);
                }
            }
        }

        [ImportingConstructor]
        public AppViewModel(IWindowManager windowManager, IModel model, IDialogService dialog, IServiceAdapter read)
        {
            _windowManager = windowManager;
            _model = model;
            _dialogService = dialog;
            _readService = read;

            _dialogService.Filter = "json (*.json)|*.json";

            Init();
        }

        private void Init()
        {
            Time = _model.Time;
            Interval = _model.Interval.ToString();
            Path = _model.Path;
        }

        public void Select()
        {
            if (_dialogService.SelectFolderDialog())
            {
                Path = _dialogService.FilePath;
            }
        }

        public void New()
        {
            _model.Reset();
            Init();
        }

        public void Open()
        {
            if (_dialogService.OpenFileDialog())
            {
                _readService.Open(_dialogService.FilePath);
                Init();
            }
        }

        public void Save()
        {
            if (_dialogService.SaveFileDialog())
            {
                _model.Time = Time;
                _model.Path = Path;
                var i = 0;

                if(int.TryParse(Interval, out i))
                {
                    _model.Interval = i;
                }
                
                _readService.Save(_dialogService.FilePath);
            }
        }

        public void Start()
        {
            var windowSettings = new Dictionary<string, object>()
            {
                {"WindowState", System.Windows.WindowState.Maximized},
                {"SizeToContent", System.Windows.SizeToContent.Manual},
                {"WindowStyle", System.Windows.WindowStyle.None}
            };

            var result = _windowManager.ShowDialog(new PresentationViewModel(this), null, windowSettings);
        }
    }
}
