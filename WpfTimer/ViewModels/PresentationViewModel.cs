﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using Caliburn.Micro;
using WpfTimer.Services;
using WpfTimer.Views;

namespace WpfTimer.ViewModels
{
    public class PresentationViewModel : Screen
    {
        private AppViewModel _appViewModel;
        private DispatcherTimer _worldTime;
        private NavigationList<string> _files;

        private int _seconds;
        public int Seconds
        {
            get { return _seconds; }
            set
            {
                if (_seconds != value)
                {
                    _seconds = value;
                    NotifyOfPropertyChange(() => Seconds);
                }
            }
        }

        private string _pathToImage;
        public string PathToImage
        {
            get { return _pathToImage; }
            set
            {
                if (_pathToImage != value)
                {
                    _pathToImage = value;
                    NotifyOfPropertyChange(() => PathToImage);
                }
            }
        }

        public PresentationViewModel(AppViewModel appViewModel)
        {
            _appViewModel = appViewModel;
                 
            _worldTime = new DispatcherTimer();
            _worldTime.Interval = TimeSpan.FromSeconds(1.0);
            _worldTime.Tick += _worldTime_Tick;

            _files = new NavigationList<string>();

            PathToImage = "";
            foreach (var file in Directory.GetFiles(_appViewModel.Path))
            {
                _files.Add(file);
            }

            PathToImage = _files.Current;

            Seconds = 0;

            _worldTime.Start();
        }

        private void _worldTime_Tick(object sender, EventArgs e)
        {
            Seconds++;
        }

        public void NextImage()
        {
            PathToImage = _files.MoveNext;

            SimpleLoger.Write(string.Format("{0}, {1}", Seconds, PathToImage));
        }

        public void Close()
        {
            TryClose(true);
        }
    }
}
