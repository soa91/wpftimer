﻿namespace WpfTimer.Interfaces
{
    public interface IModel
    {
        string Time { get; set; }

        int Interval { get; set; }

        string Path { get; set; }

        void Reset();
    }
}