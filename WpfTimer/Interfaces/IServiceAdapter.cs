﻿namespace WpfTimer.Interfaces
{
    public interface IServiceAdapter
    {
        void Open(string filePath);
        void Save(string filePath);
    }
}