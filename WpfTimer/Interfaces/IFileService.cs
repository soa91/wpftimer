﻿namespace WpfTimer.Interfaces
{
    public interface IFileService<T>
    {
        T Open(string fileName);
        void Save(string fileName, T reps);
    }
}