﻿namespace WpfTimer.Interfaces
{
    public interface IDialogService
    {
        string FilePath { get; set; }
        string Filter { get; set; }
        bool OpenFileDialog();
        bool SelectFolderDialog();
        bool SaveFileDialog();
        void ShowMessage(string message);
    }
}